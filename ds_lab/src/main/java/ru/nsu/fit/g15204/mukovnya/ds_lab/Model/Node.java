package ru.nsu.fit.g15204.mukovnya.ds_lab.Model;

import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.Map;
import com.vladmihalcea.hibernate.type.basic.PostgreSQLHStoreType;


@Data
@Entity
@Table(name="nodes")
@TypeDef(name = "hstore", typeClass = PostgreSQLHStoreType.class)
public class Node {
    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false)
    private Long id;

    @Column
    private Double version;

    @Column
    private String timestamp;

    @Column
    private Long changeset;

    @Column
    private Long uid;

    @Column
    private String username;

    @Column
    private double lat;

    @Column
    private double lon;

    @Type(type = "hstore")
    @Column(columnDefinition = "hstore")
    private Map<String, String> tags;
}

