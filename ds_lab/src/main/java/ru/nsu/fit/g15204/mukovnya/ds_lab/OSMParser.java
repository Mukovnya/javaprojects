package ru.nsu.fit.g15204.mukovnya.ds_lab;

import ru.nsu.fit.g15204.mukovnya.ds_lab.Model.Node;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OSMParser {
    private XMLStreamReader reader;

    public OSMParser(String relativePath) throws IOException, XMLStreamException {
        InputStream is = Files.newInputStream(Paths.get(relativePath));
        XMLInputFactory factory = XMLInputFactory.newInstance();
        this.reader = factory.createXMLStreamReader(is);
    }

    public List<Node> readNodes(int count) throws XMLStreamException {
        ArrayList nodes = new ArrayList<Node>();

        for (int i = 0; i < count; i++) {
            Node node = this.readNextNode(reader);
            if (node == null) {
                break;
            }
            nodes.add(node);
        }
        return nodes;
    }


    private Node readNextNode(XMLStreamReader reader) throws XMLStreamException {
        reader.next();

        if (reader.getEventType() == XMLEvent.START_ELEMENT) {
            Node node = new Node();

            int attributesNum = reader.getAttributeCount();
            for (int i = 0; i < attributesNum; i++) {
                String attributeName = reader.getAttributeLocalName(i);
                String attributeValue = reader.getAttributeValue(i);

                switch(attributeName) {
                    case "id":
                        node.setId(Long.parseLong(attributeValue));
                        break;
                    case "version":
                        node.setVersion(Double.parseDouble(attributeValue));
                        break;
                    case "timestamp":
                        node.setTimestamp(attributeValue);
                        break;
                    case "changeset":
                        node.setChangeset(Long.parseLong(attributeValue));
                        break;
                    case "uid":
                        node.setUid(Long.parseLong(attributeValue));
                        break;
                    case "user":
                        node.setUsername(attributeValue);
                        break;
                    case "lat":
                        node.setLat(Double.parseDouble(attributeValue));
                        break;
                    case "lon":
                        node.setLon(Double.parseDouble(attributeValue));
                        break;
                }
            }

            Map tags = this.readTags(reader, "node");
            node.setTags(tags);

            return node;
        }
        return null;
    }

    private Map<String, String> readTags(XMLStreamReader reader, String mainElement) throws XMLStreamException {
        Map<String, String> tags = new HashMap<>();

        while (!reader.isEndElement() || !mainElement.equals(reader.getLocalName())) {
            if (reader.isStartElement() && "tag".equals(reader.getLocalName())) {
                String key = reader.getAttributeValue(0);
                String value = reader.getAttributeValue(1);

                tags.put(key, value);
            }
            reader.next();
        }
        return tags;
    }
}
