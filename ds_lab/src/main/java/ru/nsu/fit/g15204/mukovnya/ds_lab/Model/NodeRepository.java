package ru.nsu.fit.g15204.mukovnya.ds_lab.Model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NodeRepository extends CrudRepository<Node, Long> {
    @Query(value = "SELECT *, earth_distance(ll_to_earth(?1,?2), ll_to_earth(lat, lon)) as node_distance " +
                   "FROM nodes WHERE earth_box(ll_to_earth(?1,?2), ?3) @> ll_to_earth(lat, lon) " +
                   "ORDER by node_distance;",
            nativeQuery = true)
    public List<Node> findAllByRadius(Double lat, Double lon, Double radius);
}