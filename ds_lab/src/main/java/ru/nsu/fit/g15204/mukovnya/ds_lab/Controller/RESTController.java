package ru.nsu.fit.g15204.mukovnya.ds_lab.Controller;

import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.g15204.mukovnya.ds_lab.Model.Node;
import ru.nsu.fit.g15204.mukovnya.ds_lab.Model.NodeRepository;
import ru.nsu.fit.g15204.mukovnya.ds_lab.OSMParser;
import java.util.List;
import java.util.Optional;

@RestController
public class RESTController {
    private NodeRepository nodeRepository;

    RESTController(NodeRepository nodeRepository){
        this.nodeRepository = nodeRepository;
    }

    @GetMapping("/nodes")
    public List<Node> getAllNodes() {
        return (List<Node>)this.nodeRepository.findAll();
    }

    @GetMapping("/nodes/{id}")
    public Optional<Node> getNode(@PathVariable long id) {
        return this.nodeRepository.findById(id);
    }

    @GetMapping("/find")
    public List<Node> findInRadius(
            @RequestParam Double lat,
            @RequestParam Double lon,
            @RequestParam Double radius) {
        return nodeRepository.findAllByRadius(lat, lon, radius);
    }

    @PostMapping("/nodes")
    public Node createNode(@RequestBody Node node) {
        return nodeRepository.save(node);
    }

    @DeleteMapping("/nodes")
    public void deleteAllNodes() {
        this.nodeRepository.deleteAll();
    }

    @DeleteMapping("/nodes/{id}")
    public void deleteNode(@PathVariable long id) {
        Optional<Node> node = this.nodeRepository.findById(id);
        if (node.isPresent()) {
            this.nodeRepository.deleteById(id);
        }
    }

    @PostMapping("/parse")
    public void parseOSM(@RequestParam(name = "path") String path) {
        try{
            OSMParser parser = new OSMParser(path);
            while (true) {
                List<Node> nodes = parser.readNodes(1000);
                nodeRepository.saveAll(nodes);
                if (nodeRepository.count() > 5000)
                    break;
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
