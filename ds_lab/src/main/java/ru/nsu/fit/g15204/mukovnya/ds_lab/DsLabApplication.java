package ru.nsu.fit.g15204.mukovnya.ds_lab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DsLabApplication {
	public static void main(String[] args) {
		SpringApplication.run(DsLabApplication.class, args);
	}

}
